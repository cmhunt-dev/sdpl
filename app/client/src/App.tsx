import './styles/styles.scss';

import * as React from 'react';
import { hot } from 'react-hot-loader';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import ScrollToTop from './components/ScrollToTop';
import Home from './components/home';
import DivisionHome from './components/division-home';
import Fixtures from './components/fixtures';
import Team from './components/team';

interface Props {

}

class App extends React.Component<Props, {}> {

  render() {
    return (
      <Router>
        <ScrollToTop>
          <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light"  data-toggle="collapse" data-target="#navbarSupportedContent">
              <a className="navbar-brand" href="#">SDPL</a>
              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>

              <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                  <li className="nav-item active">
                    <Link className="nav-link" to="/">Home</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/div/p/">Prem</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/div/1/">Div 1</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/div/2/">Div 2</Link>
                  </li>
                  <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#" id="fixturesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Fixtures
                    </a>
                    <div className="dropdown-menu" aria-labelledby="fixturesDropdown">
                      <Link className="nav-link" to="/fix/p/">Prem</Link>
                      <Link className="nav-link" to="/fix/1/">Div 1</Link>
                      <Link className="nav-link" to="/fix/2/">Div 2</Link>
                    </div>
                  </li>
                </ul>
              </div>
            </nav>
            <div className='container'>
                <Route path="/" exact component={Home} />
                <Route path="/div/p/" component={() => {
                  return <DivisionHome divId={'P'} />
                }} />
                <Route path="/div/1/" component={() => {
                  return <DivisionHome divId={'1'} />
                }} />
                <Route path="/div/2/" component={() => {
                  return <DivisionHome divId={'2'} />
                }} />
                <Route path="/fix/p/" component={() => {
                  return <div><h2>Prem Fixtures</h2><Fixtures divId={'P'} /></div> 
                }} />
                <Route path="/fix/1/" component={() => {
                  return <div><h2>Division 1 Fixtures</h2><Fixtures divId={'1'} /></div> 
                }} />
                <Route path="/fix/2/" component={() => {
                  return <div><h2>Division 2 Fixtures</h2><Fixtures divId={'2'} /></div> 
                }} />
                <Route path="/team/:team" component={({ match }: {match: any} ) => {
                  return <div><Team team={match.params.team} /></div>
                }} />
            </div>
          </div>
        </ScrollToTop>
      </Router>
    );
  }
}

export default hot(module)(App);
