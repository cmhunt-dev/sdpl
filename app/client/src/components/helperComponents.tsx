import * as React from 'react';
import { Link } from 'react-router-dom';
import { stemTeamName } from './../utils';
import { Team } from './../types';

export function TeamLink(props: { team: string } ) {
  let { team } = props;
  return (
    <Link to={`/team/${stemTeamName(team)}/`}>{team}</Link>
  )
}

export function TeamDetail(props: { team: Team }) {
  let { team } = props;
  return (
    <div>
      Division: {team.division}<br />
      Captain: {team.captain}
      <address>
        {team.venue.map((venLine, i) => {
          return <React.Fragment key={i}>{venLine} <br/></React.Fragment>
        })}
      </address>
    </div>
  )
}