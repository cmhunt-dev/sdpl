import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import PlayerForm from './PlayerForm';
import { State } from '../../types';
import { ComponentDispatchProps, ComponentStateProps, ComponentOwnProps} from './types';

const mapStateToProps = (state: State) => ({
  players: state.data.players,
  results: state.data.matches
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({

}, dispatch);

export default connect<ComponentStateProps, ComponentDispatchProps, ComponentOwnProps>(mapStateToProps, mapDispatchToProps)(PlayerForm);
