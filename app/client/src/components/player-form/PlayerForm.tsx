import * as React from 'react'
import { ComponentProps as Props } from './types'
import * as _ from 'lodash' 

export default class PlayerForm extends React.PureComponent<Props, {}> {
  render() {
    let { playerStats, results, players, highlightMatches, highlightPlayers, highlightedPlayers, highlightedMatches } = this.props
    let orderedPlayerStats = _.orderBy(playerStats, ['totalPoints'], ['desc'])
    if (results) {
      return <div className=' table-responsive'>
        <table className='table table-sm table-striped'>
          <thead>
            <tr>
              <th>Player</th>
              {results.map((result, i) => {
                let headStyle = highlightedMatches && highlightedMatches.indexOf(i) > -1 ? 'highlightCol' : ''
                // return <th>{team === stemTeamName(result.homeTeam) ? result.awayTeam : result.homeTeam}</th>
                return <th key={i} className={`text-center ${headStyle}`}>{i+1}</th>
              })}
            </tr>
          </thead>
          <tbody>
            {orderedPlayerStats.map(playerStat => {
              let regNo = playerStat.regNo
              let player = players[regNo]
              let weekPoints = 0
              let onMouseOver = null
              let onMouseOut = null
              let highlightRowStyle = ''
              if (highlightedPlayers && highlightedPlayers.indexOf(playerStat.regNo) > -1) {
                highlightRowStyle = 'highlight'
              }
              if (playerStats && highlightMatches) {
                onMouseOver = () => {
                  highlightMatches(playerStats[player.regno].weekPlayed.map(week => week-1))
                  highlightPlayers([player.regno])
                }
                onMouseOut = () => {
                  highlightMatches([])
                  highlightPlayers([])
                }
              }
              return <tr key={regNo} className={highlightRowStyle} onMouseOver={onMouseOver} onMouseOut={onMouseOut}>
                <td>{player.player}</td>
                {results.map((result, i) => {
                  let thisWeekPoints = playerStat.weekPoints[i] - weekPoints
                  weekPoints = playerStat.weekPoints[i]
                  let played = playerStat.weekPlayed.indexOf(i+1)
                  let pos = played > -1 ? playerStat.posPlayed[played] : null
                  let resultClass = thisWeekPoints > 10 ? 'result-W' : (pos !== null ? `result-L` : '')
                  let styleHighlightMatch = highlightedMatches && highlightedMatches.indexOf(i) > -1 ? 'highlightCol' : ''
                  return <td key={`${regNo}-${i}`} className={`text-center ${resultClass} ${styleHighlightMatch}`}>{pos ? `${pos} (${thisWeekPoints})` : ''}</td>
                })}
              </tr>
            })}
          </tbody>
        </table>
      </div>
    }
    return <div>Loading</div>
    
  }
}