import { Match, PlayerLookup, PlayerStats } from './../../types';
import { setFavouriteTeam } from '../../store/actions/data';

export interface ComponentStateProps {
  players?: PlayerLookup
  results?: Array<Match>
}

export interface ComponentDispatchProps {
}

export interface ComponentOwnProps {
  team: string
  playerStats?: PlayerStats
  highlightMatches?: (matches: Array<number>) => void
  highlightPlayers?: (regNos: Array<number>) => void
  highlightedPlayers: Array<number>
  highlightedMatches: Array<number>
}

export type ComponentProps = ComponentStateProps & ComponentDispatchProps & ComponentOwnProps;