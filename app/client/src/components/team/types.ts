import { Fixture, LeagueTable, Team, Match, PlayerLookup } from './../../types';
import { setFavouriteTeam } from '../../store/actions/data';

export interface ComponentStateProps {
  fixtures: { [key: string]: Array<Fixture> }
  leagueTables: { [key: string]: LeagueTable }
  teams: { [key: string]: Team }
  favTeam: string
  // Required for additional stats
  players?: PlayerLookup
  results?: Array<Match>
}

export interface ComponentDispatchProps {
  setFavouriteTeam: (team: string) => void;
}

export interface ComponentOwnProps {
  team: string,
}

export type ComponentProps = ComponentStateProps & ComponentDispatchProps & ComponentOwnProps;