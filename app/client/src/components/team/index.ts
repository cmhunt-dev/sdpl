import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import Team from './Team';
import { State } from '../../types';
import { ComponentDispatchProps, ComponentStateProps, ComponentOwnProps} from './types';
import { setFavouriteTeam } from './../../store/actions/data';

const mapStateToProps = (state: State) => ({
  fixtures: state.data.fixtures,
  leagueTables: state.data.leagueTables,
  teams: state.data.teams,
  favTeam: state.data.favTeam,
  players: state.data.players,
  results: state.data.matches
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
  setFavouriteTeam
}, dispatch);

export default connect<ComponentStateProps, ComponentDispatchProps, ComponentOwnProps>(mapStateToProps, mapDispatchToProps)(Team);
