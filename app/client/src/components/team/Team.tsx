import * as React from 'react';
import { ComponentProps as Props } from './types';
import PlayersTable from './../players-table';
import Fixtures from './../fixtures';
import Results from './../results';
import PlayerForm from '../player-form';
import { TeamDetail } from '../helperComponents';
import { IoIosHeart, IoIosHeartEmpty } from 'react-icons/io';
import { PlayerTableRow, PlayerStats } from '../../types';
import { stemTeamName, getCumulativeIndividualPoints } from '../../utils'

interface State {
  highlightMatches: Array<number>
  highlightPlayers: Array<number>
}

export default class Team extends React.PureComponent<Props, State> {

  constructor(props: Props) {
    super(props);
    this.selectFavourite = this.selectFavourite.bind(this)
    this.removeFavourite = this.removeFavourite.bind(this)
    this.highlightMatches = this.highlightMatches.bind(this)
    this.highlightPlayers = this.highlightPlayers.bind(this)
    this.state = {
      highlightMatches: [],
      highlightPlayers: []
    }
  }

  selectFavourite() {
    const { setFavouriteTeam, team } = this.props;
    setFavouriteTeam(team);
  }

  removeFavourite() {
    const { setFavouriteTeam } = this.props;
    setFavouriteTeam('');
  }

  highlightMatches(matchIdxs: Array<number>) {
    this.setState({highlightMatches: matchIdxs})
  }

  highlightPlayers(regNos: Array<number>) {
    this.setState({highlightPlayers: regNos})
  }

  render() {
    const { team: teamStem, teams, favTeam, players, results } = this.props;
    const { highlightMatches, highlightPlayers } = this.state
    const team = teams[teamStem];
    const teamPlayers = Object.values(players).filter((player: PlayerTableRow) => (stemTeamName(player.team) === teamStem))
    let playerStats: PlayerStats | null = null
    if (team) {
      let divId = team.division.toUpperCase();
      if (results) {
        playerStats = getCumulativeIndividualPoints(teamPlayers, results)
      }
      return (
        <div className='teamDiv'>
          <h2>{team.team}</h2>
          {favTeam !== teamStem &&
            <a className='favHeart' onClick={this.selectFavourite}><IoIosHeartEmpty /></a>
          }
          {favTeam === teamStem &&
            <a className='favHeart' onClick={this.removeFavourite}><IoIosHeart /></a>
          }
          <TeamDetail team={team} />
          <h3>Players</h3>
          <PlayersTable
            divId={divId}
            team={team.teamStem}
            highlightedPlayers={highlightPlayers}
            playerStats={playerStats}
            highlightMatches={this.highlightMatches}
            highlightPlayers={this.highlightPlayers}
          />
          <PlayerForm
            team={team.teamStem}
            playerStats={playerStats}
            highlightedPlayers={highlightPlayers}
            highlightedMatches={highlightMatches}
            highlightMatches={this.highlightMatches}
            highlightPlayers={this.highlightPlayers}
          />
          <h3>Results</h3>
          <Results team={team.team} highlight={highlightMatches} highlightPlayers={this.highlightPlayers} highlightMatches={this.highlightMatches} />
          <h3>Fixtures</h3>
          <Fixtures divId={divId} team={team.teamStem} />
        </div>
      ) 
    } else {
      return null;
    }

  }
}