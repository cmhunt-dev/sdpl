import * as React from 'react';
import { ComponentProps as Props } from './types';
import { LeagueTableRow } from '../../types';
import { TeamLink } from './../helperComponents';
import { stemTeamName } from './../../utils';

export default class LeagueTable extends React.PureComponent<Props, {}> {
  render() {
    const { divId, leagueTables, favTeam } = this.props;
    const table = leagueTables && leagueTables[divId] ? leagueTables[divId] : null;
    if (table) {
      return (
        <div>
          <h2>{table.name}</h2>
          <table className='table table-sm'>
            <thead>
              <tr>
                <th></th>
                <th>Team</th>
                <th className='text-right'>Pld</th>
                <th className='text-right'>W</th>
                <th className='text-right'>D</th>
                <th className='text-right'>L</th>
                <th className='text-right'>Pts</th>
              </tr>
            </thead>
            <tbody>
              {table.teams.map((team: LeagueTableRow) => {
                let rowStyle = '';
                if (favTeam === stemTeamName(team.team)) {
                  rowStyle = 'table-success'
                }
                return (
                  <tr key={team.team} className={rowStyle}>
                    <td>{team.pos}</td>
                    <td><TeamLink team={team.team} /></td>
                    <td className='text-right'>{team.played}</td>
                    <td className='text-right'>{team.won}</td>
                    <td className='text-right'>{team.drawn}</td>
                    <td className='text-right'>{team.lost}</td>
                    <td className='text-right font-weight-bold'>{team.points}</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>

      )
    } else {
      return (
        <div>Loading...</div>
      )
    }
  }
}