import * as React from 'react';

import LeagueTable from './../league-table';

interface Props {
  title: string
}

export default class Home extends React.PureComponent<Props, {}> {
  render() {
    return (
      <div>
        <div><LeagueTable divId='P' /></div>
        <div><LeagueTable divId='1' /></div>
        <div><LeagueTable divId='2' /></div>
      </div>
    )
  }
}