import { Match, PlayerLookup } from './../../types';

export interface ComponentStateProps {
  matches: null | Array<Match>
  favTeam: string
  players: PlayerLookup | undefined
}

export interface ComponentDispatchProps {
  getTeamMatches: (team: string) => void
  getDivMatches: (divId: string) => void
}

export interface ComponentOwnProps {
  team?: string
  divId?: string
  recent?: boolean
  highlight?: Array<number>
  highlightPlayers?: (players: Array<number>) => void
  highlightMatches?: (matches: Array<number>) => void
}

export type ComponentProps = ComponentStateProps & ComponentDispatchProps & ComponentOwnProps;