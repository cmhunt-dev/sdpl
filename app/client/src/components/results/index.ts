import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import Results from './Results'
import { State } from '../../types';
import { ComponentDispatchProps, ComponentStateProps, ComponentOwnProps} from './types';
import { fetchTeamMatches, fetchDivMatches } from './../../store/actions/matches';

const mapStateToProps = (state: State) => ({
  matches: state.data.matches ? state.data.matches : [],
  favTeam: state.data.favTeam,
  players: state.data.players,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
  getTeamMatches: fetchTeamMatches,
  getDivMatches: fetchDivMatches,
}, dispatch);

export default connect<ComponentStateProps, ComponentDispatchProps, ComponentOwnProps>(mapStateToProps, mapDispatchToProps)(Results);
