import * as React from 'react'
import { Game, PlayerLookup } from '../../types';

interface Props {
  games: Array<Game>
  players?: PlayerLookup
}


export default class ResultDetail extends React.PureComponent<Props, {}> {
  render() {
    let { games, players } = this.props
    return (
      <table className='table table-sm games'>
        <tbody>
          {games.map(game => {
            return <tr>
              <td>{game.homePlayer} {game.homePlayerRegNo ? <small>({players[game.homePlayerRegNo].pos})</small> : ''}</td>
              <td>{game.homeScore}</td>
              <td>{game.awayScore}</td>
              <td>{game.awayPlayer} {game.awayPlayerRegNo ? <small>({players[game.awayPlayerRegNo].pos})</small> : ''}</td>
            </tr>
          })}
        </tbody>
      </table>
    )
  }
}