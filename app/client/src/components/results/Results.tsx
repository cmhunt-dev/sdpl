import * as React from 'react'
import * as dayjs from 'dayjs'
import { ComponentProps as Props } from './types'
import { stemTeamName, addResults, getCumulativeIndividualPoints } from './../../utils'
import { TeamLink } from './../helperComponents'
import ResultDetail from './ResultDetail'

interface State {
  selectedFix: string
}
export default class Results extends React.PureComponent<Props, State> {

  constructor(props: Props) {
    super(props)
    this.state = {
      selectedFix: ''
    }
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.team !== this.props.team || prevProps.divId !== this.props.divId) {
      this.updateData()
    }
  }

  componentDidMount() {
    this.updateData()
  }

  updateData = () => {
    const { getTeamMatches, getDivMatches, team, divId } = this.props;
    if (team) {
      getTeamMatches(team)
    }
    if (divId) {
      getDivMatches(divId)
    }
  }

  selectFixture = (fixId: string) => {
    let { selectedFix } = this.state
    this.setState({
      selectedFix: fixId === selectedFix ? '' : fixId
    })
  }

  render() {
    let { matches, recent, favTeam, team, players, divId, highlight, highlightPlayers, highlightMatches } = this.props;
    let { selectedFix } = this.state;

    if (matches && matches[0]) {
      // // Checks for player league table not adding up
      // if (players) {
      //   let playersArray = Object.values(players).filter(player => player.div == divId)
      //   let cumPoints = getCumulativeIndividualPoints(Object.values(playersArray), matches)
      //   for (let i = 0; i < playersArray.length; i++) {
      //       let player: PlayerTableRow = playersArray[i]
      //       let regNo = player.regno
      //       if (player.pld != cumPoints[regNo].results.total.W + cumPoints[regNo].results.total.L) {
      //         console.log('Played total discrepancy ', player, cumPoints[regNo])
      //       }
      //       if (player.pts != cumPoints[regNo].totalPoints) {
      //         console.log('Points total discrepancy ', player, cumPoints[regNo])
      //       }
      //   }
      // }

      if (recent) {
        if (matches.length > 0) {
          let maxDate = matches[matches.length-1].date
          matches = matches.filter(match => {
            return match.date.getTime() === maxDate.getTime()
          })
        }
      }

      if (team) {
        matches = addResults(team, matches)
      }
      
      return (
        <table className='table table-sm table-striped'>
          <tbody>
            {matches.map((match, i) => {
              let highlightRowStyle = ''
              if ((favTeam === stemTeamName(match.homeTeam) || favTeam === stemTeamName(match.awayTeam)) && !team) {
                highlightRowStyle = 'table-success'
              } else if (highlight && highlight.indexOf(i) > -1) {
                highlightRowStyle = 'highlight'
              }
              let onMouseOver = null
              let onMouseOut = null
              if (team) {
                onMouseOver = () => {
                  let player: string = null
                  if (match.homeTeam === team) {
                    player = 'homePlayerRegNo'
                  } else {
                    player = 'awayPlayerRegNo'
                  }                  
                  highlightPlayers(match.games.map(game => {
                    return parseInt(game[player], 10)
                  }))
                  highlightMatches([i])
                }
                onMouseOut = () => {
                  highlightPlayers([])
                  highlightMatches([])
                }
              }

              return <React.Fragment key={match.id}>
                <tr className={`${highlightRowStyle}`} onMouseOver={onMouseOver} onMouseOut={onMouseOut}>
                  { team &&
                    <td>{i + 1}</td>
                  }
                  <td><a href="javascript:void(0);" onClick={() => {
                    this.selectFixture(match.id)
                  }}>{dayjs(match.date).format('DD/MM')}</a></td>
                  { team &&
                    <td className={`result-${match.result}`}>{match.result}</td>
                  }
                  <td><TeamLink team={match.homeTeam} /></td>
                  <td>{match.homeScore}</td>
                  <td>{match.awayScore}</td>
                  <td><TeamLink team={match.awayTeam} /></td>
                </tr>
                { selectedFix === match.id &&
                  <tr>
                    <td colSpan={7} className='gamesCell'>
                      <ResultDetail games={match.games} players={players} />
                    </td>
                  </tr>
                }
              </React.Fragment>
            })}
          </tbody>
        </table>
      );
    } else {
      return(<div>Loading...</div>);
    }
  }
}