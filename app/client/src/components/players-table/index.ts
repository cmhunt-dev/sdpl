import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import LeagueTable from './PlayersTable';
import { State } from '../../types';
import { ComponentDispatchProps, ComponentStateProps, ComponentOwnProps} from './types';

const mapStateToProps = (state: State) => ({
  playerTables: state.data.playerTables,
  favTeam: state.data.favTeam,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({

}, dispatch);

export default connect<ComponentStateProps, ComponentDispatchProps, ComponentOwnProps>(mapStateToProps, mapDispatchToProps)(LeagueTable);
