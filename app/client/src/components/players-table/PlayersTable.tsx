import * as React from 'react';
import { ComponentProps as Props } from './types';
import { stemTeamName } from './../../utils';
import { TeamLink } from './../helperComponents';
export default class PlayerTable extends React.PureComponent<Props, {}> {
  render() {
    const { divId, playerTables, team, favTeam, playerStats, highlightMatches, highlightPlayers, highlightedPlayers } = this.props;
    let table = playerTables && playerTables[divId] ? playerTables[divId] : null;
    let teamTotalWins = 0
    if (table) {
      if (team) {
        table = table.filter(row => {
          return team === stemTeamName(row.team);
        })
        if (playerStats) {
          Object.values(playerStats).map(player => {
            teamTotalWins += player.results.total['W']
          })
        }
      }
      return (
        <table className='table table-sm table-striped'>
          <thead>
            <tr>
              { team &&
                <th>Reg no.</th>
              }
              <th>Pos</th>
              <th>Player</th>
              { !team &&
                <th>Team</th>
              }
              <th className='text-right'>Pld</th>
              { team && playerStats &&
                <React.Fragment>
                  <th className='text-right'>W</th>
                  <th className='text-right'>L</th>
                  <th className='text-right'>Strk</th>
                </React.Fragment>
              }
              <th className='text-right'>Pts</th>
              <th className='text-right'>Avg</th>
              { team && playerStats &&
                <React.Fragment>
                  <th className='text-right'>% team total</th>
                </React.Fragment>
              }
            </tr>
          </thead>
          <tbody>
            {table.map(player => {
              let rowStyle = '';
              if (favTeam === stemTeamName(player.team) && !team) {
                rowStyle = 'table-success'
              }
              if (highlightedPlayers && highlightedPlayers.indexOf(player.regno) > -1) {
                rowStyle += ' highlight'
              }
              let onMouseOver = null
              let onMouseOut = null
              if (team && playerStats && highlightMatches) {
                onMouseOver = () => {
                  highlightMatches(playerStats[player.regno].weekPlayed.map(week => week-1))
                  highlightPlayers([player.regno])
                }
                onMouseOut = () => {
                  highlightMatches([])
                  highlightPlayers([])
                }
              }
              return (
                <tr key={player.player} className={rowStyle} onMouseOver={onMouseOver} onMouseOut={onMouseOut} >
                  { team &&
                    <td>{player.regno}</td>
                  }
                  <td>{player.pos}</td>
                  <td>{player.player}</td>
                  { !team &&
                    <td><TeamLink team={player.team} /></td>
                  }
                  <td className='text-right'>{player.pld}</td>
                  { team && playerStats &&
                    <React.Fragment>
                      <td className='text-right'>{playerStats[player.regno].results.total['W']}</td>
                      <td className='text-right'>{playerStats[player.regno].results.total['L']}</td>
                      <td className='text-right'>{playerStats[player.regno].lastResult ? `${playerStats[player.regno].streak[playerStats[player.regno].lastResult]}${playerStats[player.regno].lastResult}` : ''}</td>
                    </React.Fragment>
                  }
                  <td className='text-right font-weight-bold'>{player.pts}</td>
                  <td className='text-right'>
                    {player.pld && player.pts &&
                      (player.pts / player.pld).toFixed(2)
                    }
                  </td>
                  { team && playerStats &&
                    <React.Fragment>
                      <td className='text-right'>
                        {(teamTotalWins > 0 ? playerStats[player.regno].results.total['W'] / teamTotalWins * 100 : 0).toFixed(1)}%
                      </td>
                    </React.Fragment>
                  }
                </tr>
              )
            })}
          </tbody>
        </table>
      )
    } else {
      return (
        <div>Loading...</div>
      )
    }
  }
}