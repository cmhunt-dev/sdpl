import { PlayerTableRow, PlayerStats } from './../../types';

export interface ComponentStateProps {
  playerTables: { [key: string]: Array<PlayerTableRow> },
  favTeam: string
}

export interface ComponentDispatchProps {

}

export interface ComponentOwnProps {
  divId: string
  team?: string
  // Required for additional team stats
  playerStats?: PlayerStats | null
  highlightMatches?: (matches: Array<number>) => void
  highlightPlayers?: (matches: Array<number>) => void
  highlightedPlayers?: Array<number>
}

export type ComponentProps = ComponentStateProps & ComponentDispatchProps & ComponentOwnProps;