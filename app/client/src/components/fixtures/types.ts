import { Fixture } from './../../types';

export interface ComponentStateProps {
  fixtures: { [key: string]: Array<Fixture> }
  favTeam: string
}

export interface ComponentDispatchProps {
}

export interface ComponentOwnProps {
  divId: string
  team?: string
  next?: boolean
}

export type ComponentProps = ComponentStateProps & ComponentDispatchProps & ComponentOwnProps;