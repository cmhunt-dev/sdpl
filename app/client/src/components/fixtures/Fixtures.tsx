import * as React from 'react';
import { ComponentProps as Props } from './types';
import { Fixture } from '../../types';
import * as dayjs from 'dayjs';
import { stemTeamName } from './../../utils';
import { TeamLink } from './../helperComponents';
export default class Fixtures extends React.PureComponent<Props, {}> {
  render() {
    const { divId, fixtures: allFixtures, team, favTeam, next } = this.props;
    let fixtures = allFixtures && allFixtures[divId] ? allFixtures[divId] : null;
    if (fixtures) {
      if (team) {
        fixtures = fixtures.filter(fix => {
          return team === stemTeamName(fix.homeTeam) || team === stemTeamName(fix.awayTeam)
        });
      }
      let now = new Date()
      fixtures = fixtures.filter(fix => {
        return fix.date > now
      })

      if (next && fixtures) {
        let minDate = fixtures[0].date.getTime()
        fixtures = fixtures.filter(fix => {
          return fix.date.getTime() === minDate
        })
      }

      let currentRound: number | null = null;
      let highlightRound: number | null = null
      let currentDate = dayjs().subtract(1, 'day').toDate();
      return (
        <table className='table table-sm table-striped'>
          <tbody>
            {fixtures.map((fix: Fixture, i: number) => {
              let highlightRowStyle = ''
              if ((favTeam === stemTeamName(fix.homeTeam) || favTeam === stemTeamName(fix.awayTeam)) && !team) {
                highlightRowStyle = 'table-success'
              }
              if (currentRound !== fix.round && !next) {
                if (fix.date > currentDate && highlightRound === null) {
                  highlightRound = fix.round
                }
                currentRound = fix.round;
              }
              return (
                <tr className={`table-${highlightRound === fix.round ? 'info' : ''} ${highlightRowStyle}`} key={i + '-' + fix.homeTeam}>
                  <td>{dayjs(fix.date).format('DD/MM')}</td>
                  <td><TeamLink team={fix.homeTeam} /></td>
                  <td><TeamLink team={fix.awayTeam} /></td>
                </tr>
              )
            })}
          </tbody>
        </table>
      )
    } else {
      return <div>Loading...</div>
    }
  }
}