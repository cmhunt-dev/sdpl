import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import Fixtures from './Fixtures';
import { State } from '../../types';
import { ComponentDispatchProps, ComponentStateProps, ComponentOwnProps} from './types';

const mapStateToProps = (state: State) => ({
  fixtures: state.data.fixtures,
  favTeam: state.data.favTeam,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({

}, dispatch);

export default connect<ComponentStateProps, ComponentDispatchProps, ComponentOwnProps>(mapStateToProps, mapDispatchToProps)(Fixtures);
