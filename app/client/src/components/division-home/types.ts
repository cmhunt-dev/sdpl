import { LeagueTable } from './../../types';

export interface ComponentStateProps {
  leagueTables: { [key: string]: LeagueTable },
}

export interface ComponentDispatchProps {

}

export interface ComponentOwnProps {
  divId: string
}

export type ComponentProps = ComponentStateProps & ComponentDispatchProps & ComponentOwnProps;