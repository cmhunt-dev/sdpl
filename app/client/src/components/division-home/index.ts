import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import DivisionHome from './DivisionHome';
import { State } from '../../types';
import { ComponentDispatchProps, ComponentStateProps, ComponentOwnProps} from './types';

const mapStateToProps = (state: State) => ({
  leagueTables: state.data.leagueTables
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({

}, dispatch);

export default connect<ComponentStateProps, ComponentDispatchProps, ComponentOwnProps>(mapStateToProps, mapDispatchToProps)(DivisionHome);
