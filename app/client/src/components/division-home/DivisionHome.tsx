import * as React from 'react';
import { ComponentProps as Props } from './types';
import LeagueTable from './../league-table';
import PlayerTable from './../players-table';
import Results from '../results';
import Fixtures from '../fixtures';

export default class DivisionHome extends React.PureComponent<Props, {}> {
  render() {
    const { divId } = this.props; 
    return (
      <div>
        <LeagueTable divId={divId} />
        <h3>Recent results</h3>
        <Results divId={divId} recent={true}/>
        <h3>Next fixtures</h3>
        <Fixtures divId={divId} next={true} />
        <h3>Player table</h3>
        <PlayerTable divId={divId} />
      </div>
    )
  }
}