import { Match, PlayerTableRow, PlayerStats } from "./types";

export function stemTeamName(teamName: string): string {
  return teamName.toLowerCase()
    .replace('the', '')
    .trim()
    .split(' ')
    .join('-')
}

export function addResults (team: string, matches: Array<Match>) {
  team = stemTeamName(team)
  return matches.map(match => {
    let result = null
    if (match.homeScore || match.awayScore) {
      if (team === stemTeamName(match.homeTeam)) {
        result = match.homeScore > match.awayScore ? 'W' : (match.awayScore > match.homeScore ? 'L' : 'D')
      } else if (team === stemTeamName(match.awayTeam)) {
        result = match.homeScore > match.awayScore ? 'L' : (match.awayScore > match.homeScore ? 'W' : 'D')
      }
    }
    match.result = result
    return match
  })
}

export function getCumulativeIndividualPoints(players: Array<PlayerTableRow> , matches: Array<Match>) {
  let cumTotal: PlayerStats = {}
  players.map(player => {
    cumTotal[player.regno] = {
      regNo: player.regno,
      totalPoints: 0,
      weekPoints: [],
      weekPlayed: [],
      posPlayed: [],
      lastResult: null,
      streak: {
        W: 0,
        L: 0
      },
      results: {
        total: {
          W: 0,
          L: 0
        },
        H: {
          W: 0,
          L: 0
        },
        A: {
          W: 0,
          L: 0
        }
      },
      scores: {
        total: {
          '2-0': 0,
          '2-1': 0,
          '1-2': 0,
          '0-2': 0
        },
        H: {
          '2-0': 0,
          '2-1': 0,
          '1-2': 0,
          '0-2': 0
        },
        A: {
          '2-0': 0,
          '2-1': 0,
          '1-2': 0,
          '0-2': 0
        }
      },
    }
  })
  for (let i = 0; i < matches.length; i++) {
    let match = matches[i]
    for (let j = 0; j < match.games.length; j++) {
      let game = match.games[j]
      let result
      if (cumTotal[game.homePlayerRegNo]) {
        result = getGamePoints(game.homeScore, game.awayScore)
        let lastResult = cumTotal[game.homePlayerRegNo].lastResult
        cumTotal = addResultToCumTotal(game.homePlayerRegNo, cumTotal, 'H', result,  j+1, i+1, lastResult)
        cumTotal[game.homePlayerRegNo].lastResult = result[1]
      }
      if (cumTotal[game.awayPlayerRegNo]) {
        result = getGamePoints(game.awayScore, game.homeScore)
        let lastResult = cumTotal[game.awayPlayerRegNo].lastResult
        cumTotal = addResultToCumTotal(game.awayPlayerRegNo, cumTotal, 'H', result, j+1, i+1, lastResult)
        cumTotal[game.awayPlayerRegNo].lastResult = result[1]        
      }
    }
    Object.keys(cumTotal).map(regNo => {
      cumTotal[regNo].weekPoints.push(cumTotal[regNo].totalPoints)
    })
  }
  return cumTotal
}

function addResultToCumTotal(regNo: number, cumTotal: PlayerStats, ven: string, result: [number, string, string], pos: number, week: number, lastRes: string | null) {
  cumTotal[regNo].totalPoints += result[0]
  cumTotal[regNo].results.total[result[1]] ++
  cumTotal[regNo].results[ven][result[1]] ++
  cumTotal[regNo].scores.total[result[2]] ++
  cumTotal[regNo].scores[ven][result[2]] ++
  cumTotal[regNo].posPlayed.push(pos)
  cumTotal[regNo].weekPlayed.push(week)
  if (lastRes !== result[1]) {
    cumTotal[regNo].streak = {W: 0, L: 0}
  }
  cumTotal[regNo].streak[result[1]] ++  
  return cumTotal
}

function getGamePoints(myGames: number, hisGames: number): [number, 'W' | 'L', '2-0' | '2-1' | '1-2' |  '0-2'] {
  if (myGames === 2 && hisGames ===0) {
    return [20, 'W', '2-0']
  } else if (myGames === 2 && hisGames === 1) {
    return [15, 'W', '2-1']
  } else if (myGames === 1) {
    return [5, 'L', '1-2']
  } else {
    return [0, 'L', '0-2']
  }
}