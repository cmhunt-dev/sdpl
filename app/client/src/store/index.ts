import { createStore, applyMiddleware, Middleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import combinedReducer from './reducers';
import config from './../config';

let middleware: Array<Middleware> = [thunk];
if (config.mode === 'development') {
  middleware.push(logger);
}

/* eslint-disable no-underscore-dangle */
export default createStore(
  combinedReducer,
  applyMiddleware(...middleware),
);
/* eslint-enable */