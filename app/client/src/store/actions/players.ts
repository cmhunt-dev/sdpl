import {
  SET_PLAYER_TABLE,
  SET_PLAYER_LOOKUP
} from './actionTypes'
import { Action, PlayerTableRow, Fixture, LeagueTableRow, Team, Match } from './../../types'
import config from './../../config'
import { Dispatch } from 'redux'

export const setPlayerTable = (divId: string, table: Array<PlayerTableRow>): Action => (
  {
    type: SET_PLAYER_TABLE,
    payload: {
      id: divId,
      table
    },
  }
)

export const setPlayerLookup = (lookup: any): Action => (
  {
    type: SET_PLAYER_LOOKUP,
    payload: lookup
  }
)

export const fetchAllPlayersTables = () => {
  let lookup: any = {}
  return (dispatch: Dispatch) => {
    let url = config.apiBaseUrl + 'player';
    fetch(url)
      .then((response: Response) => {
        return response.json()
      })
      .then(data => {
        let tables: any = {}
        data.map((player: any) => {
          if (!tables[player['div']]) {
            tables[player['div']] = []
          }
          tables[player['div']].push(player)
          lookup[player.regno] = player
        })
        dispatch(setPlayerLookup(lookup))
        Object.keys(tables).map((tableId: string) => {
          let table = tables[tableId].sort((a: PlayerTableRow,b: PlayerTableRow) => {
            if (a.pos > b.pos) {
              return 1
            } else {
              return -1
            }
          })
          dispatch(setPlayerTable(tableId, table))
        })
      })
      .catch(ex => {
        console.log(ex)
      })
  }
}
