import {
  SET_LEAGUE_TABLE,
  SET_PLAYER_TABLE,
  SET_DIVISION_FIXTURES,
  SET_TEAMS,
  SET_FAV_TEAM,
} from './actionTypes'
import { Action, PlayerTableRow, Fixture, LeagueTableRow, Team, Match } from './../../types'
import config from './../../config';
import { Dispatch } from 'redux';
import * as dayjs from 'dayjs';
import { stemTeamName } from './../../utils';


export const setFavouriteTeam = (team: string): Action => {
  localStorage.setItem('favTeam', team);
  return {
    type: SET_FAV_TEAM,
    payload: team,
  }
}


export const setLeagueTable = (divId: string, table: Array<LeagueTableRow>): Action => (
  {
    type: SET_LEAGUE_TABLE,
    payload: {
      id: divId,
      table
    },
  }
);

export const setFixtures = (divId: string, fixtures: Array<Fixture>): Action => (
  {
    type: SET_DIVISION_FIXTURES,
    payload: {
      id: divId,
      fixtures
    }
  }
)

export const fetchLeagueTable = (divId: string) => {
  return (dispatch: Dispatch) => {
    let url = config.apiBaseUrl + `table?`  + encodeURIComponent(`query={"short":"${divId}"}`);
    fetch(url)
      .then((response: Response) => {
        return response.json();
      })
      .then(data => {
        dispatch(setLeagueTable(divId, data))
      })
      .catch(ex => {
        console.log(ex)
      })
  }
}

export const fetchAllLeagueTables = () => {
  return (dispatch: Dispatch) => {
    let url = config.apiBaseUrl + 'table';
    fetch(url)
      .then((response: Response) => {
        return response.json();
      })
      .then(data => {
        data.map((div: any) => {
          dispatch(setLeagueTable(div.short, div))
        })
      })
      .catch(ex => {
        console.log(ex)
      })
  }
}

export const fetchAllFixtures = () => {
  return (dispatch: Dispatch) => {
    let url = config.apiBaseUrl + 'fixture?sort={"division": 1, "date": 1, "homeTeam": 1}'
    fetch(url)
      .then((response: Response) => {
        return response.json();
      })
      .then(data => {
        let divs: Array<string> = [];
        let amendedFixtures = data.map((fix: Fixture) => {
          if (divs.indexOf(fix.division) === -1) {
            divs.push(fix.division);
          }
          fix.date = dayjs(fix.date).toDate();
          return fix;
        })
        divs.map(div => {
          let fixtures = amendedFixtures.filter((fix:Fixture) => {
            return div === fix.division
          })
          dispatch(setFixtures(div, fixtures));
        })
      })
      .catch(ex => {
        console.log(ex)
      })
  }
}

export const fetchAllTeams = () => {
  return (dispatch: Dispatch) => {
    let url = config.apiBaseUrl + `team`;
    fetch(url)
      .then((response: Response) => {
        return response.json();
      })
      .then(data => {
        let amendedData: { [key: string]: Team } = {}
        data.map((teamIn: any) => {
          let teamStem = stemTeamName(teamIn.team);
          amendedData[teamStem] = {
            team: teamIn.team,
            teamStem,
            division: teamIn.division,
            captain: teamIn.captain,
            venue: teamIn.venue,
          }
        })
        dispatch({
          type: SET_TEAMS,
          payload: amendedData,
        })
      })
      .catch(ex => {
        console.log(ex)
      })
  }
}
