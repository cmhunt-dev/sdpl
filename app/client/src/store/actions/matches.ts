import {
  SET_MATCHES,
} from './actionTypes'
import { Match } from './../../types'
import config from './../../config';
import { Dispatch } from 'redux';

let teamMatches: any = {}
export const fetchTeamMatches = (teamName: string) => {
  return (dispatch: Dispatch) => {
    if (teamMatches[teamName]) {
      dispatch({
        type: SET_MATCHES,
        payload: teamMatches[teamName],
      })
    } else {
      let query = encodeURIComponent(`{"$or":[{"homeTeam":"${teamName}"},{"awayTeam":"${teamName}"}]}`)
      let url = config.apiBaseUrl + `match?query=${query}&sort=date`;
      fetch(url)
        .then((response: Response) => {
          return response.json();
        })
        .then(data => {
          data = populateMatches(data)
          teamMatches[teamName] = data
          dispatch({
            type: SET_MATCHES,
            payload: data,
          })
        })
        .catch(ex => {
          console.log(ex)
        })
    }
  }
}

let divMatches: any = {}
export const fetchDivMatches = (div: string) => {
  return (dispatch: Dispatch) => {
    if (divMatches[div]) {
      dispatch({
        type: SET_MATCHES,
        payload: divMatches[div],
      })
    } else {
      let query = encodeURIComponent(`{"division":"${div}"}`)
      let url = config.apiBaseUrl + `match?query=${query}&sort=date`;
      fetch(url)
        .then((response: Response) => {
          return response.json();
        })
        .then(data => {
          data = populateMatches(data)
          divMatches[div] = data
          dispatch({
            type: SET_MATCHES,
            payload: data,
          })
        })
        .catch(ex => {
          console.log(ex)
        })
    }
  }
}

function populateMatches(matches: Array<Match>) {
  return matches.map((match: any) => {
    match.date = new Date(match.date)
    match.id = match._id
    return match;
  })
}