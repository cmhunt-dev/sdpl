const SET_LEAGUE_TABLE = 'SET_LEAGUE_TABLE'
const SET_PLAYER_TABLE = 'SET_PLAYER_TABLE'
const SET_DIVISION_FIXTURES = 'SET_DIVISION_FIXTURES'
const SET_TEAMS = 'SET_TEAMS'
const SET_FAV_TEAM = 'SET_FAV_TEAM'
const SET_MATCHES = 'SET_MATCHES'
const SET_PLAYER_LOOKUP = 'SET_PLAYER_LOOKUP'

export {
    SET_LEAGUE_TABLE,
    SET_PLAYER_TABLE,
    SET_DIVISION_FIXTURES,
    SET_TEAMS,
    SET_FAV_TEAM,
    SET_MATCHES,
    SET_PLAYER_LOOKUP,
};
