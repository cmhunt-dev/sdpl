import {
  SET_LEAGUE_TABLE,
  SET_PLAYER_TABLE,
  SET_DIVISION_FIXTURES,
  SET_TEAMS,
  SET_FAV_TEAM,
  SET_MATCHES,
  SET_PLAYER_LOOKUP
} from '../actions/actionTypes';
import { AppData } from '../../types';
  
const initialState: AppData = {
  leagueTables: {},
  playerTables: {},
  fixtures: {},
  teams: {},
  favTeam: null,
  matches: null,
  players: {},
};
  
export default function dataReducer(state: any = initialState, action: any) {
  switch (action.type) {
    case SET_LEAGUE_TABLE:
      let leagueTables: any = { ...state.leagueTables }
      leagueTables[action.payload.id] = action.payload.table
      return { ...state, leagueTables };
    case SET_PLAYER_TABLE:
      let playerTables: any = { ...state.playerTables }
      playerTables[action.payload.id] = action.payload.table
      return { ...state, playerTables };
    case SET_DIVISION_FIXTURES:
      let fixtures: any = { ...state.fixtures }
      fixtures[action.payload.id] = action.payload.fixtures
      return { ...state, fixtures };
    case SET_TEAMS:
      return { ...state, teams: action.payload };
    case SET_FAV_TEAM:
      return { ...state, favTeam: action.payload };
    case SET_MATCHES:
      return { ...state, matches: action.payload };
    case SET_PLAYER_LOOKUP:
      return { ...state, players: action.payload };
    default:
      return state;
  }
}
  