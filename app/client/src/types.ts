export interface Config {
  mode: 'live' | 'development'
  apiBaseUrl: string;
}

export interface Action {
  type: string
  payload: any
}

export interface AppData {
  leagueTables?: { [key: string]: LeagueTable }
  playerTables?: { [key: string]: Array<PlayerTableRow> }
  players?: PlayerLookup
  fixtures?: { [key: string]: Array<Fixture>}
  teams?: { [key: string]: Team }
  favTeam?: string | null
  matches: null | Array<Match>
}

export interface PlayerLookup { [regNo: string]: PlayerTableRow }

export interface State {
  data: AppData
}

/* Data types */
export interface LeagueTableRow {
  pos: number
  team: string
  played: number
  won: number
  drawn: number
  lost: number
  points: number
}
export interface LeagueTable {
  short: string
  name: string
  teams: Array<LeagueTableRow>
}

export interface PlayerTableRow {
  div?: string
  pos: number
  regno: number
  player: string
  team: string
  pld: number
  pts: number
}

export interface WinLoseCount {
  W: number
  L: number
  [res: string]: number
}

export interface ScoreCount {
  '2-0': number
  '2-1': number
  '1-2': number
  '0-2': number
  [score: string]: number
}

export interface PlayerStats {
  [regNo: string]: {
    regNo: number
    totalPoints: number
    weekPoints: Array<number>
    weekPlayed: Array<number>
    posPlayed: Array<number>
    streak: WinLoseCount
    lastResult: string | null
    results: {
      total: WinLoseCount
      H: WinLoseCount
      A: WinLoseCount
      [ven: string]: WinLoseCount
    },
    scores: {
      total: ScoreCount
      H: ScoreCount
      A: ScoreCount
      [ven: string]: ScoreCount
    },
  }
}

export interface Fixture {
  division: string
  round: number
  date: Date
  homeTeam: string
  awayTeam: string
}

export interface Team {
  team: string
  teamStem: string
  division: string
  captain: string
  venue: Array<string>
}

export interface Game {
  homePlayer: string
  homePlayerRegNo?: number
  homeScore: number
  awayPlayer: string
  awayPlayerRegNo?: number
  awayScore: number
  [key: string]: any
}

export interface Match {
  id: string
  division: string
  round?: number
  date: Date
  homeTeam: string
  homeScore?: number
  awayTeam: string
  awayScore?: number
  games: Array<Game>
  // When a match is looked at from context of a team
  result?: string | null
}
