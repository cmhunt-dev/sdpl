import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AnyAction } from 'redux';
import { Provider } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import 'bootstrap';
import App from './App';
import store from './store'
import {
    fetchAllLeagueTables,
    fetchAllFixtures,
    fetchAllTeams,
    setFavouriteTeam
} from './store/actions/data';

import {
    fetchAllPlayersTables
} from './store/actions/players'

(store.dispatch as ThunkDispatch<{}, void, AnyAction>)(fetchAllLeagueTables());
(store.dispatch as ThunkDispatch<{}, void, AnyAction>)(fetchAllPlayersTables());
(store.dispatch as ThunkDispatch<{}, void, AnyAction>)(fetchAllFixtures());
(store.dispatch as ThunkDispatch<{}, void, AnyAction>)(fetchAllTeams());
let favTeam = localStorage.getItem('favTeam');
if (favTeam) {
    (store.dispatch as ThunkDispatch<{}, void, AnyAction>)(setFavouriteTeam(favTeam));
}

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
    , document.getElementById('app'));
