import { Config } from './types'

let config: Config = {
    // mode: 'live',
    mode: 'development',
    // apiBaseUrl: '/api/v1/',
    apiBaseUrl: 'http://localhost:3001/api/v1/',
}

export default config;