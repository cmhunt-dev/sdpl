import { Config } from './types'

let config: Config = {
    mode: 'live',
    apiBaseUrl: '/api/v1/',
}

export default config;