const webpack = require('webpack');

const commonPaths = require('./paths');
module.exports = {
  mode: 'development',
  devtool: 'source-map',
  output: {
    filename: '[name].js',
    path: commonPaths.outputPath,
    chunkFilename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        use: [
          'style-loader',
          {
            loader: 'typings-for-css-modules-loader',
            options: {
              sourceMap: true,
              modules: true,
              namedExport: true,
              camelCase: true,
              //localIdentName: '[local]___[hash:base64:5]'
              localIdentName: '[local]'
            }
          },
          'sass-loader'
        ]
      }
    ]
  },
  devServer: {
    publicPath: '/',
    contentBase: commonPaths.outputPath,
    compress: true,
    hot: true,
    historyApiFallback: true
  },
  plugins: [new webpack.HotModuleReplacementPlugin()]
};
