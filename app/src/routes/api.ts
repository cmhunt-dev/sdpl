import express, {Request, Response, NextFunction} from 'express'
var router = express.Router()
const restify = require('express-restify-mongoose')
var expressListRoutes = require('express-list-routes')

let restrictWrites = {
  preCreate: (req: Request, res: Response, next: NextFunction) => {
    return res.sendStatus(403)
  },
  preUpdate: (req: Request, res: Response, next: NextFunction) => {
    return res.sendStatus(403)
  },
  preDelete: (req: Request, res: Response, next: NextFunction) => {
    return res.sendStatus(403)
  }
}

restify.serve(router, require('./../models/Player'), {
  name: 'player',
  totalCountHeader: true,
  ...restrictWrites
})

restify.serve(router, require('./../models/Team'), {
  name: 'team',
  totalCountHeader: true,
  ...restrictWrites
})

restify.serve(router, require('./../models/Fixture'), {
  name: 'fixture',
  totalCountHeader: true,
  ...restrictWrites
})

restify.serve(router, require('./../models/Table'), {
  name: 'table',
  totalCountHeader: true,
  ...restrictWrites
})

restify.serve(router, require('./../models/Match'), {
  name: 'match',
  totalCountHeader: true,
  ...restrictWrites
})

expressListRoutes('API:', router)

module.exports = router