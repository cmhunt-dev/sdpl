import {
  insertFixtures,
  insertTeams,
  insertPlayers,
  insertTables,
  insertResults,
} from './importData'
import { logger } from './logger';

var cron = require('node-cron')

// Set up the web app
require('./models/App');

// Set up cron to import data regularly
cron.schedule("0 * * * *", function () {
  importAll()
}, null, true, 'Europe/London')

importAll()

function importAll() {
  logger.info("Importing data")
  insertFixtures()
  insertTeams()
  insertPlayers()
  insertTables()
  insertResults()
}