var mongoose = require('mongoose')
var Schema = mongoose.Schema

var fixtureSchema = new Schema({
  division: String,
  round: Number,
  date: Date,
  homeTeam: String,
  awayTeam: String,
}, {
  timestamps: true
})

module.exports = mongoose.model('Fixture', fixtureSchema)
