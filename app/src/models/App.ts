import path from 'path'

function app () {
  var mongoose = require('mongoose')
  mongoose.Promise = global.Promise
  const { config } = require('./../config')
  mongoose.connect(config.database.connection, { useNewUrlParser: true, useCreateIndex: true })

  var express = require('express')
  var cors = require('cors')
  var winston = require('winston')
  var expressWinston = require('express-winston')

  var app = express()

  app.use(cors())

  app.use(expressWinston.logger({
    transports: [
      new winston.transports.Console({
        colorize: true
      })
    ],
    meta: true, // optional: control whether you want to log the meta data about the request (default to true)
    msg: 'HTTP {{req.method}} {{req.url}}', // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
    expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
    colorize: true, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
  }))

  // Load in api routes
  app.use(require('./../routes/api'))

  app.use(express.static(path.join(__dirname, './../../build')))

  app.get('*',function(req: any,res: any){
    res.sendFile(path.join(__dirname, './../../build', 'index.html'));
  });

  app.listen(config.port, function () {
    winston.info('App listening on port 3001')
  })
}

module.exports = app()
  