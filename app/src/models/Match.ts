var mongoose = require('mongoose')
var Schema = mongoose.Schema

var matchSchema = new Schema({
  division: String,
  round: Number,
  date: Date,
  homeTeam: String,
  homeScore: Number,
  awayTeam: String,
  awayScore: Number,
  games: [{
    homePlayer: String,
    homePlayerRegNo: String,
    homeScore: Number,
    awayPlayer: String,
    awayPlayerRegNo: String,
    awayScore: Number
  }]
}, {
  timestamps: true
})

module.exports = mongoose.model('Match', matchSchema)
