var mongoose = require('mongoose')
var Schema = mongoose.Schema

var teamSchema = new Schema({
  team: String,
  division: String,
  captain: String,
  venue: [String],
}, {
  timestamps: true
})

module.exports = mongoose.model('Team', teamSchema)
