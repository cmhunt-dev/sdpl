var mongoose = require('mongoose')
var Schema = mongoose.Schema

var playerSchema = new Schema({
  div: String,
  pos: Number,
  regno: Number,
  player: String,
  team: String,
  pld: Number,
  pts: Number
}, {
  timestamps: true
})

module.exports = mongoose.model('Player', playerSchema)
