var mongoose = require('mongoose')
var Schema = mongoose.Schema

var tableSchema = new Schema({
  short: String,
  name: String,
  teams: [{
    pos: Number,
    team: String,
    played: Number,
    won: Number,
    drawn: Number,
    lost: Number,
    points: Number
  }]
}, {
  timestamps: true
})

module.exports = mongoose.model('table', tableSchema)
