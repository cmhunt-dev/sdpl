export interface DivisionConfig {
  name: string,
  short: string,
  data: {
    table: string,
    fixtures: string,
    players: string,
    results: string,
  }
} 

export interface Config {
  port: number,
  database: {
      connection: string,
      test: boolean
  },
  urlBase: string,
  data: {
      teams: string
  },
  divisions: Array<DivisionConfig>
}

export interface Game {
  homePlayer: string,
  homePlayerRegNo?: string,
  homeScore: number,
  awayPlayer: string,
  awayPlayerRegNo?: string,
  awayScore: number
}

export interface Match {
  division: string,
  round?: number,
  date?: Date,
  homeTeam: string,
  homeScore?: number,
  awayTeam: string,
  awayScore?: number,
  games: Array<Game>
}