import { Config } from 'types'

const dbServer: string | undefined = process.env.DB_SERVER
if (!dbServer && process.title !== 'browser') {
  throw new Error('DB_SERVER env var must be set')
}

export const config: Config = {
  port: process.env.PORT ? parseInt(process.env.PORT, 10) : 3001,
  database: {
    connection: dbServer ? dbServer : '',
    test: dbServer ? dbServer.includes('localhost') : false,
  },
  urlBase: "http://www.sd-pl.co.uk/",
  data: {
    teams: "/table/tables/teamdetails.csv"
  },
  divisions: [
    {
      name: "Premier Division",
      short: "P",
      data: {
        table: "tables/premierteam.csv",
        fixtures: "tables/premfixtures.csv",
        players: "premp/tables/premplayers.csv",
        results: "premr/tables/premresults.csv"
      },
    },
    {
      name: "Division 1",
      short: "1",
      data: {
        table: "tables/div1table.csv",
        fixtures: "tables/div1fixtures.csv",
        players: "div1p/tables/div1players.csv",
        results: "div1r/tables/div1results.csv",
      },
    },
    {
      name: "Division 2",
      short: "2",
      data: {
        table: "tables/div2table.csv",
        fixtures: "tables/div2fixtures.csv",
        players: "div2p/tables/div2players.csv",
        results: "div2r/tables/div2results.csv",
      },
    },
  ],
}
