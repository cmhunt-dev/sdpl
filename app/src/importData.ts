import { DivisionConfig, Match as IMatch } from "types";
import csv from 'csvtojson'
import request from 'request'
import { promisify } from 'util';
import { config } from "./config";
import { logger } from './logger';
const Table = require('./models/Table');
const Player = require('./models/Player');
const Team = require('./models/Team')
const Fixture = require('./models/Fixture')
const Match = require('./models/Match')
const requestAsync: any = promisify(request)
const moment = require('moment')
var mongoose = require('mongoose')
mongoose.connect(config.database.connection, { useNewUrlParser: true, useCreateIndex: true })

const path = config.urlBase

interface KeyValLookup {
  [key: string]: string
}

const playersLookup: KeyValLookup = {}

async function loadCSV(path: string, headers: boolean = true): Promise<any> {
  let response = await requestAsync(path)
  return csv({
    noheader: !headers
  })
    .fromString(response.body)
    .then((jsonObj: object)=>{
      return jsonObj;
    })
}

function getDivisionConfig(divCode: string): DivisionConfig {
  return config.divisions.filter((div: DivisionConfig) => {
    return div.short === divCode;
  })[0]
}

function loadPlayers(divCode: string) {
  let division = getDivisionConfig(divCode);
  let fullPath = path + division.data.players

  return loadCSV(fullPath)
    .then(data => {
      return data.map((player: any, i: number) => {
        let playerOut: any = {}
        Object.keys(player).map((key: string, i: number) => {
          playerOut[key.toLowerCase().replace(' ', '')] = player[key] === '-' ? '0' : player[key];
        })
        return playerOut
      })
    })
}


function loadResults(divCode: string) {
  let division = getDivisionConfig(divCode)
  let fullPath = path + division.data.results

  return loadCSV(fullPath, false)
    .then(data => {
      let matches = []
      let match: IMatch | null = null
      for (let i = 0; i < data.length; i++) {
        let row = data[i]
        // Skip empty rows
        if (row['field1'] === '' || (row['field2'] === '' && row['field8'] === '')) {
          continue
        }

        // Start new match
        if (match === null) {
          match = {
            division: divCode,
            date: moment(row['field1'], 'DD-MMM-YYYY').toDate(),
            homeTeam: row['field2'],
            awayTeam: row['field8'],
            games: []
          }
        }

        if (row['field1'] !== '' && row['field3'] !== '') {
          let game = {
            homePlayer: row['field3'],
            homePlayerRegNo: playersLookup[row['field3']],
            homeScore: parseInt(row['field4'], 10),
            awayPlayer: row['field7'],
            awayPlayerRegNo: playersLookup[row['field7']],
            awayScore: parseInt(row['field6'], 10),
          }
          match.games.push(game)
        }

        if (row['field1'] !== '' && row['field3'] === '') {
          match.homeScore = parseInt(row['field4'], 10)
          match.awayScore = parseInt(row['field6'], 10)
          matches.push(match)
          match = null
        }
      }
      return matches
    })
    
}

function loadFixtures(divCode: string) {
  let division = getDivisionConfig(divCode)
  let fullPath = path + division.data.fixtures

  return loadCSV(fullPath)
    .then(data => {
      let currentRound: string = '';

      let amendedData = data.map((fix: any) => {
        let amendedFix: any = {}
        Object.keys(fix).map((key: string) => {
          amendedFix[key.toLowerCase()] = fix[key]
        })
        return amendedFix
      })


      return amendedData.map((fix: any) => {
        if (fix['away teams'] === '' && fix['home teams'] !== '') {
          currentRound = fix['home teams']
        }
        return {
          round: currentRound.replace('Round ', ''),
          date: fix['field1'],
          homeTeam: fix['home teams'],
          awayTeam: fix['away teams']
        }
      }).filter((fix: any) => {
        return fix.awayTeam !== ''
      })
    })
}

function loadLeagueTable(divCode: string) {
  let division = getDivisionConfig(divCode);
  let fullPath = path + division.data.table

  return loadCSV(fullPath)
    .then(data => {
      return data.map((team: any, i: number) => {
        let teamOut: any = {
          pos: i + 1
        }
        Object.keys(team).map((key: string, i: number) => {
          if (i === 0) {
            teamOut['team'] = team[key];
          } else {
            teamOut[key.toLowerCase()] = team[key] === '-' ? '0' : team[key];
          }
        })
        return teamOut
      })
      .filter((team: any) => {
        return team.team !== ''
      })
    })
}

export function insertTables() {
  Table.deleteMany({}, (err: any, obj: any) => {
    config.divisions.map(div => {
      loadLeagueTable(div.short)
        .then(data => {
          let table = new Table({
            short: div.short,
            name: div.name,
            teams: data
          })
          table.save((err: any, obj: any) => {
            if (err) {
              console.log(err)
            }
          })
        })
    })

  })
}

export function insertPlayers() {
  logger.info('Loading players')
  Player.deleteMany({}, () => {
    config.divisions.map(div => {
      loadPlayers(div.short)
        .then(data => {
          logger.debug(`Got data from division ${div.short}`)
          data.filter((player: any) => {return player.regno !== ''}).map((player: any, i: number) => {
            player.div = div.short
            player.pos = i + 1
            playersLookup[player.player] = player.regno
            let playerObj = new Player(player)
            logger.debug(playerObj)
            playerObj.save()
          })
        })
    })
  })
}

async function loadTeams(): Promise<any> {
  let fullPath = path + config.data.teams
  let data = await loadCSV(fullPath)
  let teams = data.map((team: any) => {
    let ven = Object.keys(team).filter((key: any, i: number) => {
      if (i >= 3) {
        return true;
      }
    }).map((key: any, i: number) => {
        return team[key];
    })
    return {
      team: team.Team,
      division: team.Division,
      captain: team.Captain,
      venue: ven
    }
  })
  return teams;
}

export function insertTeams() {
  Team.deleteMany({}, () => {
    loadTeams()
      .then((teams: any) => {
        teams.map((team: any) => {
          let teamObj = new Team(team)
          teamObj.save()
        })
      })
  })
}

export function insertFixtures() {
  Fixture.deleteMany({}, () => {
    config.divisions.map(div => {
      loadFixtures(div.short)
        .then(data => {
          data.map((fix: any, i: number) => {
            fix.division = div.short
            let fixObj = new Fixture(fix)
            fixObj.save()
          })
        })
    })
  })
}

export function insertResults() {
  Match.deleteMany({}, () => {
    config.divisions.map(div => {
      loadResults(div.short)
        .then(data => {
          data.map((match: any, i: number) => {
            let matchObj = new Match(match)
            matchObj.save()
          })
        })
    })
  })
}
