# Setup our base image with yarn
FROM node:alpine as base
RUN npm i -g yarn

FROM base as client
# Install and build client
WORKDIR /usr/src/app
COPY ./app/client ./
RUN mv /usr/src/app/src/config_prod.ts /usr/src/app/src/config.ts
RUN npm i -g yarn
RUN yarn
RUN yarn build

# Install server
FROM base as server
WORKDIR /usr/src/app
COPY ./app ./
RUN rm -rf client && yarn

COPY --from=client /usr/src/build ./build

# Add wait exe. This will enable us to wait for mongo to be up
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.2.1/wait /wait
RUN chmod +x /wait

EXPOSE 80
WORKDIR /usr/src/app

CMD /wait && yarn start

